import { Action } from '@ngrx/store';
import { Student } from './student.model';

export enum StudentActions {
  ADD_STUDENT = "ADD_STUDENT",      // Adds a new student
  REMOVE_STUDENT = "REMOVE_STUDENT"  // Removes a student
}

export class AddStudentAction implements Action {
  readonly type = StudentActions.ADD_STUDENT;
  payload : Student;
}

