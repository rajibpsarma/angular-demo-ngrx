import { Action } from "@ngrx/store";

// Takes the current state and copied it to new state based on Action.
export function simpleReducer(state : string, action : Action) {
  console.log("In reducer, action : " + action.type + ", state : " + state);

  let str = "";
  switch(action.type) {
    case "FRENCH":
      str = "Bonjour le monde";
      break;
    case "SPANISH":
      str = "Hola Mundo";
      break;
    default:
      str = "Hello World";
  }
  console.log("str = " + str);
  return str;
}
