# Angular NgRx demo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

It shows how to use NgRx with Angular.

## The steps to be followed are: ##
* clone repository "git clone https://rajibpsarma@bitbucket.org/rajibpsarma/angular-demo-NgRx.git"
* ng new angular-demo-NgRx
* cd angular-demo-NgRx
* npm install @ngrx/store --save
* ng serve
* Now explore the app using "http://localhost:4200/"