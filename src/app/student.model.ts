export class Student {
  name:string;
  id : number;
  stream : string; // science / arts / ...
  constructor(id, name, stream) {
    this.id = id;
    this.name = name;
    this.stream = stream;
  }
}
