import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from "@ngrx/store";
import { Student } from './student.model';
import { StudentActions, AddStudentAction } from './student.actions';
/*
interface AppState {
  message : string;
}
*/
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // key "studentList" is from app.module.ts
  // value is from reducer's initialState
  private store : Store<{studentList : {students : Student []}}>;
  students : Observable<{students : Student []}>;

  constructor(store : Store<{studentList : {students : Student []}}>) {
    this.store = store;
  }

  ngOnInit() {
    this.students = this.store.select("studentList");
  }

  addStudent(studentId, studentName, studentStream) {
    console.log(studentId + ", " + studentName + ", " + studentStream);
    let student = new Student(studentId, studentName, studentStream);
    let action = new AddStudentAction();
    action.payload = student;
    this.store.dispatch(action);
  }

  /*
  message : Observable<string>;

  constructor(private store : Store<AppState>) {
    this.message = this.store.select("message");
  }

  onFrenchMsg() {
    this.store.dispatch({type:"FRENCH"});
  }

  onSpanishMsg() {
    this.store.dispatch({type:"SPANISH"});
  }
  */
}
