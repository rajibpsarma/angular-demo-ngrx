import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { simpleReducer } from "./simple.reducer";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentReducer } from './student.reducer';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //StoreModule.forRoot({message:simpleReducer})
    StoreModule.forRoot({studentList : StudentReducer})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
