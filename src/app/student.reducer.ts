import { Student } from './student.model';
import { StudentActions, AddStudentAction } from './student.actions';
import { Action } from '@ngrx/store';

// Some Initial state
const initialState = {
  students : [
    new Student("SC22", "Rajib Sarma", "Science"),
    new Student("AR01", "Sanjib Sarmah", "Arts")
  ]
}

// state: old state
export function StudentReducer(state = initialState, action:AddStudentAction) {
  switch(action.type) {
    case StudentActions.ADD_STUDENT: // Use upper case as convention
      // add the new data to old data and return
      return {
        students : [
          ...state.students,  // The old data
          action.payload      // add the new data
        ]
      };

    default:
      return state;
  }
}
